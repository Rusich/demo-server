<template>
  <div class="todo-list">
    <ul class="list-group">
      <li
        class="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
        v-for="item in todoList" :key="item.id"
      >
        <div class="item-wrap d-flex justify-content-between align-items-center w-100">
          <div class="item-title pl-3">
            <input
              type="checkbox"
              class="form-check-input"
              id="exampleCheck1"
              @click="setCompleted(item)"
              :checked="item.compleeted">
            <label
              class="form-check-label"
              :class="{completed: item.completed}"
              for="exampleCheck1"
            >{{item.title}}</label>
            <div class="item-desc">{{item.description}}</div>
          </div>
          <div class="item-action">
            <button type="button" class="btn btn-success m-0" @click="openModal(item)">
              <i class="fas fa-edit"></i>
            </button>
            <button type="button" class="btn btn-danger m-0 ml-2" @click="removeTodo(item.id)">
              <i class="far fa-trash-alt"></i>
            </button>
          </div>
        </div>
      </li>
    </ul>

    <div class="modal-overlay" :class="{active: showDetails}">
      <div class="modal" :class="{active: showDetails}" v-if="showDetails && editableTodo">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">
                Редактирование задачи
              </h5>
              <button type="button" class="close" @click="closeModal">
                <span>&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input type="text" class="form-control mb-2" id="title" placeholder="Заголовок" v-model="editableTodo.title">
              <textarea class="form-control" id="desc" rows="3" placeholder="Описание" v-model="editableTodo.description"></textarea>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" @click="closeModal">Отменить</button>
              <button type="button" class="btn btn-primary" @click="editTodo">Сохранить</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<script>
export default {
  name: 'TodoList',
  data() {
    return {
      showDetails: false,
      editableTodo: null,
    }
  },
  computed: {
    todoList() {
      return this.$store.state.todoList || [];
    }
  },
  methods: {
    setCompleted(todo) {
      this.$store.dispatch('updateTodo', { ...todo, completed: !todo.completed });
    },
    editTodo() {
      this.$store.dispatch('updateTodo', this.editableTodo)
        .then(() => {
          this.closeModal();
        });
    },
    removeTodo(id) {
      this.$store.dispatch('removeTodo', id);
    },
    closeModal() {
      this.editableTodo = null;
      this.showDetails = false;
    },
    openModal(todo) {
      this.editableTodo = { ...todo };
      this.showDetails = true;
    }
  }
}
</script>

<style lang="css">
  .modal.active {
    display: block;
  }
  .completed {
    text-decoration: line-through;
  }
  .modal-overlay {
    position: absolute;
    display: none;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: rgba(0, 0, 0, 0.4);
  }
  .modal-overlay.active {
    display: block;
  }
  .item-desc {
    color: #bbb;
  }
</style>
