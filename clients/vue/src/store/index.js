import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    todoList: [],
    bookList: [],
    categoryList: [],
    authorList: [],
  },
  actions: {
    getBookList({ commit }) {
      commit('setField', { name: 'bookList', value: [] });
      Vue.axios.get('book/list')
        .then(({data}) => {
          commit('setField', { name: 'bookList', value: data });
        });
    },
    getAuthorList({ commit }) {
      commit('setField', { name: 'authorList', value: [] });
      Vue.axios.get('author/list')
        .then(({data}) => {
          commit('setField', { name: 'authorList', value: data });
        });
    },
    getCategoryList({ commit }) {
      commit('setField', { name: 'categoryList', value: [] });
      Vue.axios.get('book-category/list')
        .then(({data}) => {
          commit('setField', { name: 'categoryList', value: data });
        });
    },
    setBook({ dispatch }, data) {
      return new Promise((resolve, reject) => {
        Vue.axios.post('book/create', data)
          .then((response) => {
            dispatch('getBookList');
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
    setAuthor({ dispatch }, data) {
      return new Promise((resolve, reject) => {
        Vue.axios.post('author/create', data)
          .then((response) => {
            dispatch('getAuthorList');
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
    setCategory({ dispatch }, data) {
      return new Promise((resolve, reject) => {
        Vue.axios.post('book-category/create', data)
          .then((response) => {
            dispatch('getCategoryList');
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
    removeBook({ dispatch }, id) {
      return new Promise((resolve, reject) => {
        Vue.axios.delete(`book/${id}`)
          .then((response) => {
            dispatch('getBookList');
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
    removeAuthor({ dispatch }, id) {
      return new Promise((resolve, reject) => {
        Vue.axios.delete(`author/${id}`)
          .then((response) => {
            dispatch('getAuthorList');
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
    removeCategory({ dispatch }, id) {
      return new Promise((resolve, reject) => {
        Vue.axios.delete(`book-category/${id}`)
          .then((response) => {
            dispatch('getCategoryList');
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
    updateBook({ dispatch }, data) {
      return new Promise((resolve, reject) => {
        Vue.axios.patch(`book/update/${data.id}`, data)
          .then((response) => {
            dispatch('getBookList');
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
    updateAuthor({ dispatch }, data) {
      return new Promise((resolve, reject) => {
        Vue.axios.patch(`author/update/${data.id}`, data)
          .then((response) => {
            dispatch('getAuthorList');
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
    updateCategory({ dispatch }, data) {
      return new Promise((resolve, reject) => {
        Vue.axios.patch(`book-category/update/${data.id}`, data)
          .then((response) => {
            dispatch('getCategoryList');
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },

    getTodoList({ commit }) {
      commit('setField', { name: 'todoList', value: [] });
      Vue.axios.get('todo/list')
        .then(({data}) => {
          commit('setField', { name: 'todoList', value: data });
        });
    },
    setTodo({ dispatch }, data) {
      return new Promise((resolve, reject) => {
        Vue.axios.post('todo/create', data)
          .then((response) => {
            dispatch('getTodoList');
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
    removeTodo({ dispatch }, id) {
      return new Promise((resolve, reject) => {
        Vue.axios.delete(`todo/${id}`)
          .then((response) => {
            dispatch('getTodoList');
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
    updateTodo({ dispatch }, data) {
      return new Promise((resolve, reject) => {
        Vue.axios.patch(`todo/update/${data.id}`, data)
          .then((response) => {
            dispatch('getTodoList');
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
  },
  mutations: {
    setField(state, payload) {
      state[payload.name] = payload.value;
    },
  },
  modules: {}
})
