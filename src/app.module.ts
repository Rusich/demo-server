import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import { ConfigModule } from '@nestjs/config';
import { TodoModule } from './todo/todo.module';
import { TodoModel } from './todo/todo.model';
import { BookModule } from './book/book.module';
import { AuthorModule } from './author/author.module';
import { BookCategoryModule } from './book-category/book-category.module';
import { AuthorModel } from "./author/author.model";
import { BookCategoryModel } from "./book-category/book-category.model";
import { BookModel } from "./book/book.model";

@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath: `.${process.env.NODE_ENV}.env` }),
    SequelizeModule.forRoot({
      dialect: 'postgres',
      host: process.env.POSTGRES_HOST,
      port: Number(process.env.POSTGRES_PORT),
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
      models: [TodoModel, AuthorModel, BookCategoryModel, BookModel],
      autoLoadModels: true,
    }),
    TodoModule,
    BookModule,
    AuthorModule,
    BookCategoryModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
