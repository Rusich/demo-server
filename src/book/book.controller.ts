import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { BookService } from './book.service';
import { CreateBookDto } from './dto/create-book.dto';

@Controller('book')
export class BookController {
  constructor(private readonly bookService: BookService) {}

  /** Создание нового автора */
  @Post('create')
  async create(@Body() dto: CreateBookDto) {
    return await this.bookService.create(dto);
  }

  /** Получение списка всех авторов */
  @Get('list')
  async getAll() {
    return await this.bookService.getAll();
  }

  /** Получение автора по айди */
  @Get(':id')
  async getById(@Param('id') id: string) {
    return await this.bookService.getById(id);
  }

  /** Обновление данных об авторе */
  @Patch('update/:id')
  async update(@Param('id') id: string, @Body() dto: CreateBookDto) {
    // return await this.bookService.update(id, dto);
  }

  /** Удаление автора из базы */
  @Delete(':id')
  async delete(@Param('id') id: string) {
    return await this.bookService.delete(id);
  }
}
