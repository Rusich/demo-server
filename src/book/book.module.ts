import { Module } from '@nestjs/common';
import { BookController } from './book.controller';
import { BookService } from './book.service';
import { BookCategoryModel } from "../book-category/book-category.model";
import { AuthorModel } from "../author/author.model";
import { SequelizeModule } from "@nestjs/sequelize";
import { BookModel } from "./book.model";

@Module({
  controllers: [BookController],
  providers: [BookService],
  imports: [
    SequelizeModule.forFeature([BookCategoryModel, AuthorModel, BookModel]),
  ]
})
export class BookModule {}
