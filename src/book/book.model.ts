import { BelongsTo, Column, DataType, ForeignKey, Model, Table } from "sequelize-typescript";
import { AuthorModel } from "../author/author.model";
import { BookCategoryModel } from "../book-category/book-category.model";

export interface IBookCreate {
  title: string;
  description: string;
  image: string;
}

@Table({ tableName: 'book' })
export class BookModel extends Model<BookModel, IBookCreate> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @Column({ type: DataType.STRING, unique: true, allowNull: false })
  title: string;

  @Column({ type: DataType.STRING, allowNull: true })
  description: string;

  @Column({ type: DataType.STRING, defaultValue: false })
  image: boolean;

  @ForeignKey(() => AuthorModel)
  @Column({ type: DataType.INTEGER, allowNull: false })
  authorId: number;

  @ForeignKey(() => BookCategoryModel)
  @Column({ type: DataType.INTEGER, allowNull: false })
  categoryId: number;

  // указываем что связь один к одному
  @BelongsTo(() => AuthorModel)
  author: AuthorModel

  // указываем что связь один к одному
  @BelongsTo(() => BookCategoryModel)
  category: BookCategoryModel
}
