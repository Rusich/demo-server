export class CreateBookDto {
  title: string;
  description: string;
  authorId: number;
  categoryId: number;
  image: string;
}
