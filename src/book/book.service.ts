import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { BookModel } from './book.model';
import { CreateBookDto } from './dto/create-book.dto';

@Injectable()
export class BookService {
  constructor(@InjectModel(BookModel) private bookModel: BookModel) {}

  async create(dto: CreateBookDto): Promise<BookModel> {
    // @ts-ignore
    return await this.bookModel.create(dto);
  }

  async getAll(): Promise<BookModel[]> {
    // @ts-ignore
    return await this.bookModel.findAll();
  }

  async getById(id: string): Promise<BookModel> {
    // @ts-ignore
    return await this.bookModel.findByPk(id);
  }

  // async update(id: string, dto: CreateBookDto): Promise<BookModel> {
    // return await this.bookModel.update(dto, { where: { id } });
  // }

  async delete(id: string): Promise<void> {
    // @ts-ignore
    return await this.bookModel.destroy({ where : { id }} );
  }
}
