import { Column, DataType, Model, Table } from 'sequelize-typescript';

/** Интерфейс объекта создания новой записи */
export interface ITodoCreate {
  title: string;
  description: string;
  completed: boolean;
}

@Table({ tableName: 'todo' })
export class TodoModel extends Model<TodoModel, ITodoCreate> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @Column({ type: DataType.STRING, unique: true, allowNull: false })
  title: string;

  @Column({ type: DataType.STRING, allowNull: true })
  description: string;

  @Column({ type: DataType.BOOLEAN, defaultValue: false })
  completed: boolean;
}
