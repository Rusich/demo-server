import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';

import { TodoModel } from './todo.model';
import { CreateTodoDto } from './dto/create-todo.dto';

@Injectable()
export class TodoService {
  constructor(@InjectModel(TodoModel) private todoModel: TodoModel) {}

  async create(dto: CreateTodoDto): Promise<TodoModel> {
    // @ts-ignore
    return await this.todoModel.create(dto);
  }

  async getAll(): Promise<TodoModel[]> {
    // @ts-ignore
    return await this.todoModel.findAll();
  }

  async getById(id: string): Promise<TodoModel> {
    // @ts-ignore
    return await this.todoModel.findByPk(id);
  }

  async updateById(id: string, dto: CreateTodoDto): Promise<TodoModel> {
    return await this.todoModel.update(dto, { where: { id } });
  }

  async deleteById(id: string) {
    // @ts-ignore
    return await this.todoModel.destroy({ where : { id }} );
  }
}
