import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';

import { TodoService } from './todo.service';
import { CreateTodoDto } from './dto/create-todo.dto';

/** Контроллер для обработки запросов к todo функуионалу */
@Controller('todo')
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  /** Создание новой задачи */
  @Post('create')
  async create(@Body() dto: CreateTodoDto) {
    return await this.todoService.create(dto);
  }

  /** Получение списка всех задач */
  @Get('list')
  async getAll() {
    return await this.todoService.getAll();
  }

  /** Получение одной задачи по ее идент. */
  @Get(':id')
  async getById(@Param('id') id: string) {
    return await this.todoService.getById(id);
  }

  /** Удаление задачи по ее айдишнику */
  @Delete(':id')
  async deleteById(@Param('id') id: string) {
    await this.todoService.deleteById(id);
  }

  /** Обновление данных в задаче по ее айдишнику */
  @Patch('update/:id')
  async updateById(@Body() dto: CreateTodoDto, @Param('id') id: string) {
    await this.todoService.updateById(id, dto);
  }
}
