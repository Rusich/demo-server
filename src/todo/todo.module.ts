import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import { TodoController } from './todo.controller';
import { TodoService } from './todo.service';
import { TodoModel } from './todo.model';

@Module({
  controllers: [TodoController],
  providers: [TodoService],
  imports: [SequelizeModule.forFeature([TodoModel])],
})
export class TodoModule {}
