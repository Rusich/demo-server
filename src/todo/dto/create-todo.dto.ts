/** Объект дто для создания записи */
export class CreateTodoDto {
  title: string;
  description: string;
  completed: boolean;
}
