/** Объект дто для создания автора книги */
export class CreateAuthorDto {
  first_name: string;
  middle_name: string;
  last_name: string;
}
