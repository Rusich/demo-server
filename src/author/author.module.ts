import { Module } from '@nestjs/common';
import { AuthorController } from './author.controller';
import { AuthorService } from './author.service';
import { BookModel } from "../book/book.model";
import { SequelizeModule } from "@nestjs/sequelize";
import { AuthorModel } from "./author.model";

@Module({
  controllers: [AuthorController],
  providers: [AuthorService],
  imports: [
    SequelizeModule.forFeature([BookModel, AuthorModel]),
  ]
})
export class AuthorModule {}
