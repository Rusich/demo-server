import { Body, Controller, Delete, Get, Param, Patch, Post } from "@nestjs/common";
import { AuthorService } from "./author.service";
import { CreateAuthorDto } from "./dto/create-author.dto";

@Controller('author')
export class AuthorController {
  constructor(private readonly authorService: AuthorService) {}

  /** Создание нового автора */
  @Post('create')
  async create(@Body() dto: CreateAuthorDto) {
    return await this.authorService.create(dto);
  }

  /** Получение списка всех авторов */
  @Get('list')
  async getAll() {
    return await this.authorService.getAll();
  }

  /** Получение автора по айди */
  @Get(':id')
  async getById(@Param('id') id: string) {
    return await this.authorService.getById(id);
  }

  /** Обновление данных об авторе */
  @Patch('update/:id')
  async update(@Param('id') id: string, @Body() dto: CreateAuthorDto) {
    return await this.authorService.update(id, dto);
  }

  /** Удаление автора из базы */
  @Delete(':id')
  async delete(@Param('id') id: string) {
    return await this.authorService.delete(id);
  }
}
