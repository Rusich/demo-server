import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { AuthorModel } from './author.model';
import { CreateAuthorDto } from './dto/create-author.dto';

@Injectable()
export class AuthorService {
  constructor(@InjectModel(AuthorModel) private authorModel: AuthorModel) {}

  async create(dto: CreateAuthorDto): Promise<AuthorModel> {
    // @ts-ignore
    return await this.authorModel.create(dto);
  }

  async getAll(): Promise<AuthorModel[]> {
    // @ts-ignore
    return await this.authorModel.findAll();
  }

  async getById(id: string): Promise<AuthorModel> {
    // @ts-ignore
    return await this.authorModel.findByPk(id);
  }

  async update(id: string, dto: CreateAuthorDto): Promise<AuthorModel> {
    return await this.authorModel.update(dto, { where: { id } });
  }

  async delete(id: string): Promise<void> {
    // @ts-ignore
    return await this.authorModel.destroy({ where : { id }} );
  }
}
