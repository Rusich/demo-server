import { Column, DataType, HasMany, Model, Table } from "sequelize-typescript";
import { BookModel } from "../book/book.model";

/** Интерфейс для соазания автора книги */
export interface IAuthorCreate {
  first_name: string;
  middle_name: string;
  last_name: string;
}

@Table({ tableName: 'author' })
export class AuthorModel extends Model<AuthorModel, IAuthorCreate> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @Column({ type: DataType.STRING, allowNull: false })
  first_name: string;

  @Column({ type: DataType.STRING, allowNull: false })
  middle_name: string;

  @Column({ type: DataType.STRING, allowNull: false })
  last_name: string;

  @HasMany(() => BookModel)
  books: BookModel[];
}
