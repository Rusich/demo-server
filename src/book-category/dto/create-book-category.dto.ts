/** Объект дто для создания категории книг */
export class CreateBookCategoryDto {
  title: string;
  description?: string;
}
