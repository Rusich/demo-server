import { Module } from '@nestjs/common';
import { BookCategoryController } from './book-category.controller';
import { BookCategoryService } from './book-category.service';
import {SequelizeModule} from "@nestjs/sequelize";
import { BookModel } from "../book/book.model";
import { BookCategoryModel } from "./book-category.model";

@Module({
  controllers: [BookCategoryController],
  providers: [BookCategoryService],
  imports: [
    SequelizeModule.forFeature([BookModel, BookCategoryModel]),
  ]
})
export class BookCategoryModule {}
