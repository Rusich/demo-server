import { Column, DataType, HasMany, Model, Table } from 'sequelize-typescript';
import { BookModel } from '../book/book.model';

/** Интерфейс для соазания категории книги */
export interface IBookCategoryCreate {
  title: string;
  description?: string;
}

@Table({ tableName: 'book-category' })
export class BookCategoryModel extends Model<BookCategoryModel, IBookCategoryCreate> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @Column({ type: DataType.STRING, allowNull: false, unique: true })
  title: string;

  @Column({ type: DataType.STRING, allowNull: false })
  description: string;

  @HasMany(() => BookModel)
  books: BookModel[];
}
