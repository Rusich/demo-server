import { Body, Controller, Delete, Get, Param, Patch, Post } from "@nestjs/common";
import { BookCategoryService } from "./book-category.service";
import { CreateBookCategoryDto } from "./dto/create-book-category.dto";

@Controller('book-category')
export class BookCategoryController {
  constructor(private readonly bookCategoryService: BookCategoryService) {}

  /** Создание новогй катагории */
  @Post('create')
  async create(@Body() dto: CreateBookCategoryDto) {
    return await this.bookCategoryService.create(dto);
  }

  /** Получение списка всех категорий */
  @Get('list')
  async getAll() {
    return await this.bookCategoryService.getAll();
  }

  /** Получение катагории по айди */
  @Get(':id')
  async getById(@Param('id') id: string) {
    return await this.bookCategoryService.getById(id);
  }

  /** Обновление данных о катагории */
  @Patch('update/:id')
  async update(@Param('id') id: string, @Body() dto: CreateBookCategoryDto) {
    return await this.bookCategoryService.update(id, dto);
  }

  /** Удаление катагории из базы */
  @Delete(':id')
  async delete(@Param('id') id: string) {
    return await this.bookCategoryService.delete(id);
  }
}
