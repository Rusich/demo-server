import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { BookCategoryModel } from './book-category.model';
import { CreateBookCategoryDto } from './dto/create-book-category.dto';

@Injectable()
export class BookCategoryService {
  constructor(@InjectModel(BookCategoryModel) private categoryModel: BookCategoryModel) {}

  async create(dto: CreateBookCategoryDto): Promise<BookCategoryModel> {
    // @ts-ignore
    return await this.categoryModel.create(dto);
  }

  async getAll(): Promise<BookCategoryModel[]> {
    // @ts-ignore
    return await this.categoryModel.findAll();
  }

  async getById(id: string): Promise<BookCategoryModel> {
    // @ts-ignore
    return await this.categoryModel.findByPk(id);
  }

  async update(id: string, dto: CreateBookCategoryDto): Promise<BookCategoryModel> {
    return await this.categoryModel.update(dto, { where: { id } });
  }

  async delete(id: string): Promise<void> {
    // @ts-ignore
    return await this.categoryModel.destroy({ where : { id }} );
  }
}
